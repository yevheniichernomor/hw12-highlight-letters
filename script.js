/* Відповіді:

Тому що зараз існує багато інших способів вводу даних в поле окрім клавіатури. Доцільніше 
використовувати подію input для відслідковування будь-яких змін у полі <input>.

*/

function keyHandler(event){
    let enter = document.getElementById('enter')
    let s = document.getElementById('s');
    let e = document.getElementById('e');
    let o = document.getElementById('o');
    let n = document.getElementById('n');
    let l = document.getElementById('l');
    let z = document.getElementById('z');
    let button = document.querySelector('.btn-wrapper').children;
        
    if(event.code == 'Enter'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        enter.classList.toggle('selected');
    }

    if(event.code == 'KeyS'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        s.classList.toggle('selected');
    }
    
    if(event.code == 'KeyE'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        e.classList.toggle('selected');
    }
    
    if(event.code == 'KeyO'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        o.classList.toggle('selected');
    }
    
    if(event.code == 'KeyN'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        n.classList.toggle('selected');
    }
    
    if(event.code == 'KeyL'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        l.classList.toggle('selected');
    }
    
    if(event.code == 'KeyZ'){
        for(let child of button){
            if(child.classList.contains('selected')){
                child.classList.remove('selected');
                child.classList.add('black');
            }
           }
        z.classList.toggle('selected');
    }
}

document.addEventListener('keydown', keyHandler);